﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPInCsharp
{
    class Student:Person
    {

        private String facultate;

        public Student():base("Student implicit", 1999)
        {
            facultate = "facultate implicita";
        }

        public Student(string nume, int yearOfBirth, string facultate):base(nume, yearOfBirth)
        {
            this.facultate = facultate;
        }

        public void ConsoleWrite()
        {
            Console.WriteLine(nume + " - " + yearOfBirth + " - " + facultate);
        }
    }
}
