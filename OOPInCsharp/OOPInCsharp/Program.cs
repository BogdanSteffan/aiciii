﻿using System;

namespace OOPInCsharp
{
    class Program
    {
        static void Main(string[] args)
        {

            Person p = new Person();
            p.ConsoleWrite();

            Person q = new Person(" Person : Bogdan", 10);
            q.ConsoleWrite();

            Elev n = new Elev();
            n.ConsoleWrite();

            Elev n2 = new Elev("Tomita", 2002, 5, "Tudor Vladimirescu");
            n2.ConsoleWrite();

            Student s1 = new Student();
            s1.ConsoleWrite();

            Student s2 = new Student("Cristi", 2000, "Scoala Vietii");
            s2.ConsoleWrite();

        }
    }
}
