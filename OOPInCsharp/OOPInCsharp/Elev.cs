﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPInCsharp
{
    public class Elev : Person
    {
        private int nota;
        private string Scoala;

        public Elev() : base("Default Elev", 2010)
        {
            this.nota = 5;
            this.Scoala = "Inginerie Sibiu";
        }
        public Elev(string name, int yearOfBirth, int nota, string Scoala):base(name, yearOfBirth)
        {
            this.nota = nota;
            this.Scoala = Scoala;
        }

        public void ConsoleWrite()
        {
            Console.WriteLine(this.nume + " - " + this.yearOfBirth + " - " + this.nota + " - " + this.Scoala);
        }

        //without Export method
    }
}
