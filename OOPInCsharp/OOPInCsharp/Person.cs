﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPInCsharp
{
   public class Person
    {
        protected string nume;
        protected int yearOfBirth;

       public Person()
        {
            this.nume = "Default Person";
            this.yearOfBirth = 2000;

        }

      public  Person(string nume, int yearOfBirth)
        {
            this.nume = nume;
            this.yearOfBirth = yearOfBirth;
        }

        public void ConsoleWrite()// Without virtual momentan
        {
            Console.WriteLine("->  " + nume + " - " + yearOfBirth);
        }

        //Without Export() method;
    }
}
